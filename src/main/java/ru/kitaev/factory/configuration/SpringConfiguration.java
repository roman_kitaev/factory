package ru.kitaev.factory.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.domain.Detail;
import ru.kitaev.factory.domain.PurchaseOrder;
import ru.kitaev.factory.dto.*;

import java.util.stream.Collectors;

@Configuration
public class SpringConfiguration {
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
            .setFieldMatchingEnabled(true)
            .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE);

        //To get CustomerDTO with PurchaseOrders (without details list)
        modelMapper.typeMap(Customer.class, CustomerDTO.class).
            setConverter(ctx -> {
                    Customer sourceCustomer = ctx.getSource();
                    return new CustomerDTO(
                        sourceCustomer.getId(),
                        sourceCustomer.getName(),
                        sourceCustomer.getPurchaseOrderList().stream()
                            .map(purchaseOrder -> new PurchaseOrderDTO(
                                purchaseOrder.getId(),
                                purchaseOrder.getNumber(),
                                new CustomerDTO(
                                    sourceCustomer.getId(),
                                    sourceCustomer.getName())))
                            .collect(Collectors.toList()));
                }
            );

        //To get PurchaseOrderDTO with details and
        //Customer (without purchaseOrders list)
        modelMapper.typeMap(PurchaseOrder.class, PurchaseOrderDTO.class).
            setConverter(ctx -> {
                    PurchaseOrder sourcePurchaseOrder = ctx.getSource();
                    return new PurchaseOrderDTO(
                        sourcePurchaseOrder.getId(),
                        sourcePurchaseOrder.getNumber(),
                        new CustomerDTO(
                            sourcePurchaseOrder.getCustomer().getId(),
                            sourcePurchaseOrder.getCustomer().getName()),
                        sourcePurchaseOrder.getDetailList().stream()
                            .map(detail -> modelMapper.map(detail, DetailDTO.class))
                            .collect(Collectors.toList()));
                }
            );

        //To get DetailDTO with PurchaseOrder (without details list)
        //and Customer (without purchaseOrders list)
        modelMapper.typeMap(Detail.class, DetailDTO.class).
            setConverter(ctx -> {
                    Detail sourceDetail = ctx.getSource();
                    return new DetailDTO(
                        sourceDetail.getId(),
                        sourceDetail.getNumber(),
                        new DrawingDTO(
                            sourceDetail.getDrawing().getId(),
                            sourceDetail.getDrawing().getNumber()),
                        new PurchaseOrderDTO(
                            sourceDetail.getPurchaseOrder().getId(),
                            sourceDetail.getPurchaseOrder().getNumber(),
                            new CustomerDTO(
                                sourceDetail.getPurchaseOrder().getCustomer().getId(),
                                sourceDetail.getPurchaseOrder().getCustomer().getName())),
                        new StateDTO(
                            sourceDetail.getState().getId(),
                            sourceDetail.getState().getName()));
                }
            );

        return modelMapper;
    }
}
