package ru.kitaev.factory.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.kitaev.factory.domain.Drawing;
import ru.kitaev.factory.dto.DrawingDTO;
import ru.kitaev.factory.repository.DrawingRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DrawingService {
    private static final Logger logger = LoggerFactory.getLogger(DrawingService.class.getName());
    private final DrawingRepository drawingRepository;
    private final DetailService detailService;
    private final ModelMapper modelMapper;

    public DrawingService(DrawingRepository drawingRepository, DetailService detailService, ModelMapper modelMapper) {
        this.drawingRepository = drawingRepository;
        this.detailService = detailService;
        this.modelMapper = modelMapper;
    }

    public void save(DrawingDTO drawingDTO) {
        logger.debug("Trying to save drawing {}", drawingDTO);
        throwExceptionIfNumberIsAlreadyUsed(drawingDTO.getNumber());
        Drawing drawing = modelMapper.map(drawingDTO, Drawing.class);
        drawingRepository.save(drawing);
        logger.debug("Saved drawing {}", drawing);
    }

    public DrawingDTO get(int id) {
        logger.debug("Getting drawing by id {}", id);
        Drawing drawing = drawingRepository.findById(id)
            .orElseThrow(() -> new ServiceException("Could not find a drawing, id " + id));
        DrawingDTO drawingDTO = modelMapper.map(drawing, DrawingDTO.class);
        logger.debug("Drawing was received");
        return drawingDTO;
    }

    public List<DrawingDTO> getAll() {
        logger.debug("Getting all drawings");
        return drawingRepository.findAll().stream().
            map(drawing -> modelMapper.map(drawing, DrawingDTO.class)).
            collect(Collectors.toList());
    }

    public void updateNumber(int id, String newNumber) {
        logger.debug("Updating drawing number, id: {}", id);
        Drawing drawing = drawingRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a drawing for update, id = " + id));
        if (!drawing.getNumber().equals(newNumber)) {
            throwExceptionIfNumberIsAlreadyUsed(newNumber);
        } else {
            logger.debug("Nothing in drawing has been changed");
            return;
        }
        drawing.setNumber(newNumber);
        drawingRepository.save(drawing);
        logger.debug("Drawing was updated");
    }

    public void delete(int id) {
        logger.debug("Deleting drawing, id: {}", id);
        Drawing drawing = drawingRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a drawing to delete, id = " + id));
        if (detailService.countByDrawing(drawing) != 0) {
            throw new IllegalDeleteException("Can not delete the drawing - " +
                "you have to delete details with it first, drawing id = " + id);
        }
        drawingRepository.deleteById(id);
        logger.debug("Drawing was deleted, id: {}", id);
    }

    private void throwExceptionIfNumberIsAlreadyUsed(String number) {
        if (drawingRepository.findByNumber(number) != null) {
            throw new ServiceException("Drawing with number " + number + " already exists");
        }
    }
}
