package ru.kitaev.factory.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.kitaev.factory.domain.*;
import ru.kitaev.factory.dto.*;
import ru.kitaev.factory.repository.DetailRepository;
import ru.kitaev.factory.repository.StateRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DetailService {
    private static final Logger logger = LoggerFactory.getLogger(DetailService.class.getName());
    private final DetailRepository detailRepository;
    private final StateRepository stateRepository;
    private final ModelMapper modelMapper;

    public DetailService(DetailRepository detailRepository, StateRepository stateRepository, ModelMapper modelMapper) {
        this.detailRepository = detailRepository;
        this.stateRepository = stateRepository;
        this.modelMapper = modelMapper;
    }

    public void save(DetailDTO detailDTO) {
        logger.debug("Trying to save detail {}", detailDTO);
        throwExceptionIfNumberIsAlreadyUsed(detailDTO.getNumber());
        Detail detail = modelMapper.map(detailDTO, Detail.class);
        detailRepository.save(detail);
        logger.debug("Saved detail {}", detail);
    }

    public DetailDTO get(int id) {
        logger.debug("Getting detail by id {}", id);
        Detail detail = detailRepository.findById(id)
            .orElseThrow(() -> new ServiceException("Could not find a detail, id " + id));
        DetailDTO detailDTO = modelMapper.map(detail, DetailDTO.class);
        logger.debug("Detail was received");
        return detailDTO;
    }

    public void setState(int id, String stateName) {
        logger.debug("Changing status for detail with id {} to {}", id, stateName);
        detailRepository.findById(id)
            .orElseThrow(() -> new ServiceException("Could not find a detail, id " + id));
        State state = stateRepository.findByName(stateName);
        if (state == null) {
            throw new ServiceException("Could not find a state, name " + stateName);
        }
        detailRepository.updateState(id, state);
        logger.debug("Detail was updated");
    }

    public List<DetailDTO> getAll() {
        logger.debug("Getting all details");
        return detailRepository.findAll().stream().
            map(detail -> {
                Drawing drawing = detail.getDrawing();
                PurchaseOrder po = detail.getPurchaseOrder();
                Customer customer = po.getCustomer();
                State state = detail.getState();
                return new DetailDTO(detail.getId(), detail.getNumber(),
                    new DrawingDTO(drawing.getId(), drawing.getNumber()),
                    new PurchaseOrderDTO(po.getId(), po.getNumber(),
                        new CustomerDTO(customer.getId(), customer.getName())),
                    new StateDTO(state.getId(), state.getName()));
            }).collect(Collectors.toList());
    }

    public long countByDrawing(Drawing drawing) {
        return detailRepository.countByDrawing(drawing);
    }

    public long countByPurchaseOrder(PurchaseOrder purchaseOrder) {
        return detailRepository.countByPurchaseOrder(purchaseOrder);
    }

    public void update(DetailDTO detailDTO) {
        int id = detailDTO.getId();
        logger.debug("Updating a detail, id: {}", id);
        String newNumber = detailDTO.getNumber();
        Detail oldDetail = detailRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a detail for update, id = " + id));
        if (!oldDetail.getNumber().equals(newNumber)) {
            throwExceptionIfNumberIsAlreadyUsed(newNumber);
        }
        Detail updatedDetail = modelMapper.map(detailDTO, Detail.class);
        detailRepository.save(updatedDetail);
        logger.debug("The detail was updated");
    }

    public void delete(int id) {
        logger.debug("Deleting a detail, id: {}", id);
        Detail detail = detailRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a detail to delete, id = " + id));
        detailRepository.deleteById(id);
        logger.debug("Detail was deleted, id: {}", id);
    }

    private void throwExceptionIfNumberIsAlreadyUsed(String number) {
        if (detailRepository.findByNumber(number) != null) {
            throw new ServiceException("Detail with number " + number + " already exists");
        }
    }
}
