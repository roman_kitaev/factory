package ru.kitaev.factory.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.domain.PurchaseOrder;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.dto.PurchaseOrderDTO;
import ru.kitaev.factory.repository.PurchaseOrderRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PurchaseOrderService {
    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderService.class.getName());
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final DetailService detailService;
    private final ModelMapper modelMapper;

    public PurchaseOrderService(PurchaseOrderRepository purchaseOrderRepository, DetailService detailService,
                                ModelMapper modelMapper) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.detailService = detailService;
        this.modelMapper = modelMapper;
    }

    public void save(PurchaseOrderDTO purchaseOrderDTO) {
        logger.debug("Trying to save an order {}", purchaseOrderDTO);
        String number = purchaseOrderDTO.getNumber();
        Customer customer = modelMapper.map(purchaseOrderDTO.getCustomer(), Customer.class);
        throwExceptionIfPairOfNumberAndCustomerIsAlreadyUsed(number, customer);
        PurchaseOrder purchaseOrder = modelMapper.map(purchaseOrderDTO, PurchaseOrder.class);
        purchaseOrderRepository.save(purchaseOrder);
        logger.debug("Saved order {}", purchaseOrder);
    }

    public PurchaseOrderDTO get(int id) {
        logger.debug("Getting order by id {}", id);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findWithDetailsById(id);
        if (purchaseOrder == null) {
            throw new ServiceException("Could not find a purchase order, id " + id);
        }
        PurchaseOrderDTO purchaseOrderDTO = modelMapper.map(purchaseOrder, PurchaseOrderDTO.class);
        logger.debug("Order was received");
        return purchaseOrderDTO;
    }

    public List<PurchaseOrderDTO> getAll() {
        logger.debug("Getting all purchase orders");
        return purchaseOrderRepository.findAll().stream().
            map(po -> new PurchaseOrderDTO(po.getId(), po.getNumber(),
                new CustomerDTO(po.getCustomer().getId(), po.getCustomer().getName()))).
            collect(Collectors.toList());
    }

    public long countByCustomer(Customer customer) {
        return purchaseOrderRepository.countByCustomer(customer);
    }

    public void update(PurchaseOrderDTO purchaseOrderDTO) {
        int id = purchaseOrderDTO.getId();
        logger.debug("Updating purchase order, id: {}", id);
        String number = purchaseOrderDTO.getNumber();
        Customer customer = modelMapper.map(purchaseOrderDTO.getCustomer(), Customer.class);
        PurchaseOrder oldPurchaseOrder = purchaseOrderRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a purchase order for update, id = " + id));
        if (purchaseOrderDTO.getNumber().equals(oldPurchaseOrder.getNumber()) &&
            purchaseOrderDTO.getCustomer().getId().equals(oldPurchaseOrder.getCustomer().getId())) {
            logger.debug("Nothing in purchase order has been changed");
            return;
        } else {
            throwExceptionIfPairOfNumberAndCustomerIsAlreadyUsed(number, customer);
        }
        PurchaseOrder purchaseOrder = modelMapper.map(purchaseOrderDTO, PurchaseOrder.class);
        purchaseOrderRepository.save(purchaseOrder);
        logger.debug("Purchase order was updated");
    }

    public void delete(int id) {
        logger.debug("Deleting purchase order, id: {}", id);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a purchase order to delete, id = " + id));
        if (detailService.countByPurchaseOrder(purchaseOrder) != 0) {
            throw new IllegalDeleteException("Can not delete the purchase order - " +
                "you have to delete details for it first, purchase order id = " + id);
        }
        purchaseOrderRepository.deleteById(id);
        logger.debug("Purchase order was deleted, id: {}", id);
    }

    private void throwExceptionIfPairOfNumberAndCustomerIsAlreadyUsed(String number, Customer customer) {
        if (purchaseOrderRepository.findByNumberAndCustomer(number, customer) != null) {
            throw new ServiceException("Purchase order with number " + number +
                " and customer " + customer.getName() + " already exists");
        }
    }
}
