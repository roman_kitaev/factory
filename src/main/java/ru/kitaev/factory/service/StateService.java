package ru.kitaev.factory.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.kitaev.factory.domain.State;
import ru.kitaev.factory.dto.StateDTO;
import ru.kitaev.factory.repository.StateRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StateService {
    private static final Logger logger = LoggerFactory.getLogger(StateService.class.getName());
    private final ModelMapper modelMapper;
    private final StateRepository stateRepository;

    public StateService(StateRepository stateRepository, ModelMapper modelMapper) {
        this.stateRepository = stateRepository;
        this.modelMapper = modelMapper;
    }

    public void save(StateDTO stateDTO) {
        logger.debug("Trying to save a state {}", stateDTO);
        State state = modelMapper.map(stateDTO, State.class);
        stateRepository.save(state);
        logger.debug("Saved state {}", state);
    }

    public StateDTO getById(int id) {
        logger.debug("Getting state by id {}", id);
        State state = stateRepository.findById(id)
            .orElseThrow(() -> new ServiceException("Could not find a state, id " + id));
        StateDTO stateDTO = modelMapper.map(state, StateDTO.class);
        logger.debug("State was received");
        return stateDTO;
    }

    public StateDTO getByName(String name) {
        logger.debug("Getting state by name {}", name);
        State state = stateRepository.findByName(name);
        if (state == null) {
            throw new ServiceException("Could not find a state, name " + name);
        }
        StateDTO stateDTO = modelMapper.map(state, StateDTO.class);
        logger.debug("State was received");
        return stateDTO;
    }

    public List<StateDTO> getAll() {
        logger.debug("Getting all states");
        return stateRepository.findAll().stream().
            map(state -> modelMapper.map(state, StateDTO.class)).
            collect(Collectors.toList());
    }
}
