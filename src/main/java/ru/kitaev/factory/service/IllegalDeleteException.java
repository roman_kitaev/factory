package ru.kitaev.factory.service;

public class IllegalDeleteException extends ServiceException {
    public IllegalDeleteException() {
    }

    public IllegalDeleteException(String message) {
        super(message);
    }

    public IllegalDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalDeleteException(Throwable cause) {
        super(cause);
    }

    public IllegalDeleteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
