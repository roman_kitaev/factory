package ru.kitaev.factory.service;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.repository.CustomerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class.getName());
    private final CustomerRepository customerRepository;
    private final PurchaseOrderService purchaseOrderService;
    private final ModelMapper modelMapper;

    public CustomerService(CustomerRepository customerRepository, PurchaseOrderService purchaseOrderService,
                           ModelMapper modelMapper) {
        this.customerRepository = customerRepository;
        this.purchaseOrderService = purchaseOrderService;
        this.modelMapper = modelMapper;
    }

    public void save(CustomerDTO customerDTO) {
        logger.debug("Trying to save customer {}", customerDTO);
        throwExceptionIfNameIsAlreadyUsed(customerDTO.getName());
        Customer customer = modelMapper.map(customerDTO, Customer.class);
        customerRepository.save(customer);
        logger.debug("Saved customer {}", customer);
    }

    public CustomerDTO get(int id) {
        logger.debug("Getting customer by id {}", id);
        Customer customer = customerRepository.findWithOrdersById(id);
        if (customer == null) {
            throw new ServiceException("Could not find a customer, id " + id);
        }
        CustomerDTO customerDTO = modelMapper.map(customer, CustomerDTO.class);
        logger.debug("Costumer was received");
        return customerDTO;
    }

    public List<CustomerDTO> getAll() {
        logger.debug("Getting all customers");
        return customerRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).
            stream().
            map(customer -> new CustomerDTO(customer.getId(), customer.getName())).
            collect(Collectors.toList());
    }

    public void updateName(int id, String newName) {
        logger.debug("Updating customer name, id: {}", id);
        Customer customer = customerRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a customer for update, id = " + id));
        if (!customer.getName().equals(newName)) {
            throwExceptionIfNameIsAlreadyUsed(newName);
        } else {
            logger.debug("Nothing in customer has been changed");
            return;
        }
        customer.setName(newName);
        customerRepository.save(customer);
        logger.debug("Customer was updated");
    }

    public void delete(int id) {
        logger.debug("Deleting customer, id: {}", id);
        Customer customer = customerRepository.findById(id).
            orElseThrow(() -> new ServiceException("Could not find a customer to delete, id = " + id));
        if (purchaseOrderService.countByCustomer(customer) != 0) {
            throw new IllegalDeleteException("Can not delete the customer - " +
                "you have to delete purchase orders for it first, customer id = " + id);
        }
        customerRepository.deleteById(id);
        logger.debug("Customer was deleted, id: {}", id);
    }

    private void throwExceptionIfNameIsAlreadyUsed(String name) {
        if (customerRepository.findByName(name) != null) {
            throw new ServiceException("Customer with name " + name + " already exists");
        }
    }
}
