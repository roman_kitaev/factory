package ru.kitaev.factory.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"customer_id", "number"})})
public class PurchaseOrder {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 20, nullable = false)
    private String number;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Customer customer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchaseOrder")
    private final List<Detail> detailList = new ArrayList<>();

    public PurchaseOrder(int id, String number, Customer customer) {
        this.id = id;
        this.number = number;
        this.customer = customer;
    }

    public PurchaseOrder(String number, Customer customer) {
        this.number = number;
        this.customer = customer;
    }

    public PurchaseOrder() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Detail> getDetailList() {
        return detailList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrder purchaseOrder = (PurchaseOrder) o;
        return id == purchaseOrder.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + id +
            ", number='" + number + '\'' +
            ", customer=" + customer +
            '}';
    }
}
