package ru.kitaev.factory.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Drawing {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 20, nullable = false, unique = true)
    private String number;

    public Drawing() {
    }

    public Drawing(String number) {
        this.number = number;
    }

    public Drawing(int id, String number) {
        this.id = id;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drawing drawing = (Drawing) o;
        return id == drawing.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Drawing{" +
            "id=" + id +
            ", number='" + number + '\'' +
            '}';
    }
}
