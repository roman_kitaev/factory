package ru.kitaev.factory.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Detail {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 20, nullable = false, unique = true)
    private String number;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Drawing drawing;

    @ManyToOne
    @JoinColumn(nullable = false)
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    @JoinColumn(nullable = false)
    private State state;

    public Detail() {
    }

    public Detail(String number, Drawing drawing, PurchaseOrder purchaseOrder, State state) {
        this.number = number;
        this.drawing = drawing;
        this.purchaseOrder = purchaseOrder;
        this.state = state;
    }

    public Detail(int id, String number, Drawing drawing, PurchaseOrder purchaseOrder, State state) {
        this.id = id;
        this.number = number;
        this.drawing = drawing;
        this.purchaseOrder = purchaseOrder;
        this.state = state;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Drawing getDrawing() {
        return drawing;
    }

    public void setDrawing(Drawing drawing) {
        this.drawing = drawing;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Detail detail = (Detail) o;
        return id == detail.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Detail{" +
            "id=" + id +
            ", number='" + number + '\'' +
            ", drawing=" + drawing +
            ", purchaseOrder=" + purchaseOrder +
            ", state=" + state +
            '}';
    }
}
