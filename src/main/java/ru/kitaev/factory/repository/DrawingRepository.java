package ru.kitaev.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kitaev.factory.domain.Drawing;

public interface DrawingRepository extends JpaRepository<Drawing, Integer> {
    Drawing findByNumber(String number);
}
