package ru.kitaev.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kitaev.factory.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "SELECT c FROM Customer c LEFT JOIN FETCH c.purchaseOrderList WHERE c.id = :id")
    Customer findWithOrdersById(@Param("id") int id);

    Customer findByName(String name);
}
