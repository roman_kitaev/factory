package ru.kitaev.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.kitaev.factory.domain.Detail;
import ru.kitaev.factory.domain.Drawing;
import ru.kitaev.factory.domain.PurchaseOrder;
import ru.kitaev.factory.domain.State;

public interface DetailRepository extends JpaRepository<Detail, Integer> {
    @Modifying
    @Transactional
    @Query("UPDATE Detail d SET d.state = :state WHERE d.id = :id")
    void updateState(@Param(value = "id") int id, @Param(value = "state") State state);

    long countByDrawing(Drawing drawing);

    long countByPurchaseOrder(PurchaseOrder purchaseOrder);

    Detail findByNumber(String number);
}
