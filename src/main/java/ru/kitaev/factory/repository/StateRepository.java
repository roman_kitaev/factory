package ru.kitaev.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kitaev.factory.domain.State;

public interface StateRepository extends JpaRepository<State, Integer> {
    State findByName(String name);
}
