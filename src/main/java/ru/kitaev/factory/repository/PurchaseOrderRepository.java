package ru.kitaev.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.domain.PurchaseOrder;

import java.util.List;

public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Integer> {
    @Query(value = "SELECT po FROM PurchaseOrder po LEFT JOIN FETCH po.detailList WHERE po.id = :id")
    PurchaseOrder findWithDetailsById(@Param("id") int id);

    long countByCustomer(Customer customer);

    List<PurchaseOrder> findByCustomer(Customer customer);

    PurchaseOrder findByNumberAndCustomer(String number, Customer customer);
}
