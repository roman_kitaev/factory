package ru.kitaev.factory.dto;

import java.util.Objects;

public class DrawingDTO {
    private int id;
    private String number;

    public DrawingDTO() {
    }

    public DrawingDTO(String number) {
        this.number = number;
    }

    public DrawingDTO(int id, String number) {
        this.id = id;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrawingDTO that = (DrawingDTO) o;
        return id == that.id && Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }

    @Override
    public String toString() {
        return "DrawingDTO{" +
            "id=" + id +
            ", number='" + number + '\'' +
            '}';
    }
}
