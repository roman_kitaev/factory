package ru.kitaev.factory.dto;

import java.util.Objects;

public class DetailDTO {
    private Integer id;
    private String number;
    private DrawingDTO drawing;
    private PurchaseOrderDTO purchaseOrder;
    private StateDTO state;

    public DetailDTO() {
    }

    public DetailDTO(String number, DrawingDTO drawing, PurchaseOrderDTO purchaseOrder, StateDTO state) {
        this.number = number;
        this.drawing = drawing;
        this.purchaseOrder = purchaseOrder;
        this.state = state;
    }

    public DetailDTO(Integer id, String number, DrawingDTO drawing, PurchaseOrderDTO purchaseOrder, StateDTO state) {
        this.id = id;
        this.number = number;
        this.drawing = drawing;
        this.purchaseOrder = purchaseOrder;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public DrawingDTO getDrawing() {
        return drawing;
    }

    public void setDrawing(DrawingDTO drawing) {
        this.drawing = drawing;
    }

    public PurchaseOrderDTO getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrderDTO purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public StateDTO getState() {
        return state;
    }

    public void setState(StateDTO state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailDTO detailDTO = (DetailDTO) o;
        return Objects.equals(id, detailDTO.id) && Objects.equals(number, detailDTO.number) && Objects.equals(drawing, detailDTO.drawing) && Objects.equals(purchaseOrder, detailDTO.purchaseOrder) && Objects.equals(state, detailDTO.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, drawing, purchaseOrder, state);
    }

    @Override
    public String toString() {
        return "DetailDTO{" +
            "id=" + id +
            ", number='" + number + '\'' +
            ", drawing=" + drawing +
            ", purchaseOrder=" + purchaseOrder +
            ", state=" + state +
            '}';
    }
}
