package ru.kitaev.factory.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PurchaseOrderDTO {
    private Integer id;
    private String number;
    private CustomerDTO customer;
    private List<DetailDTO> detailList = new ArrayList<>();

    public PurchaseOrderDTO() {
    }

    public PurchaseOrderDTO(String number, CustomerDTO customer) {
        this.number = number;
        this.customer = customer;
    }

    public PurchaseOrderDTO(Integer id, String number, CustomerDTO customer) {
        this.id = id;
        this.number = number;
        this.customer = customer;
    }

    public PurchaseOrderDTO(Integer id, String number, CustomerDTO customer, List<DetailDTO> detailList) {
        this.id = id;
        this.number = number;
        this.customer = customer;
        this.detailList = detailList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public List<DetailDTO> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DetailDTO> detailList) {
        this.detailList = detailList;
    }

    public void addDetail(DetailDTO detailDTO) {
        detailList.add(detailDTO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderDTO purchaseOrderDTO = (PurchaseOrderDTO) o;
        return Objects.equals(id, purchaseOrderDTO.id) && Objects.equals(number, purchaseOrderDTO.number) && Objects.equals(customer, purchaseOrderDTO.customer) && Objects.equals(detailList, purchaseOrderDTO.detailList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, customer, detailList);
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
            "id=" + id +
            ", number='" + number + '\'' +
            ", customer=" + customer +
            '}';
    }
}
