package ru.kitaev.factory.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CustomerDTO {
    private Integer id;
    private String name;
    private List<PurchaseOrderDTO> purchaseOrderList = new ArrayList<>();

    public CustomerDTO() {
    }

    public CustomerDTO(String name) {
        this.name = name;
    }

    public CustomerDTO(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public CustomerDTO(Integer id, String name, List<PurchaseOrderDTO> purchaseOrderList) {
        this.id = id;
        this.name = name;
        this.purchaseOrderList = purchaseOrderList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PurchaseOrderDTO> getPurchaseOrderList() {
        return purchaseOrderList;
    }

    public void addPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
        purchaseOrderList.add(purchaseOrderDTO);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerDTO that = (CustomerDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(purchaseOrderList, that.purchaseOrderList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, purchaseOrderList);
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}
