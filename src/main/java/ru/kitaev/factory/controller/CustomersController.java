package ru.kitaev.factory.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.service.CustomerService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.ServiceException;

@Controller
@RequestMapping("/customers")
public class CustomersController {
    private static final Logger logger = LoggerFactory.getLogger(CustomersController.class.getName());
    private static final String REDIRECT_PATH = "redirect:/customers";
    private final CustomerService customerService;

    @Autowired
    public CustomersController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    public String showAll(Model model) {
        logger.debug("Showing all customers");
        model.addAttribute("customers", customerService.getAll());
        return "customers/all";
    }

    @GetMapping("/new")
    public String addNew(Model model) {
        logger.debug("Adding new customer");
        model.addAttribute("customer", new CustomerDTO());
        return "customers/new";
    }

    @PostMapping()
    public String create(Model model, @ModelAttribute("customer") CustomerDTO customerDTO) {
        logger.debug("Creating new customer");
        logger.trace("Customer received from front: {}", customerDTO);
        try {
            customerService.save(customerDTO);
        } catch (ServiceException e) {
            model.addAttribute("customer", customerDTO.getName());
            return "customers/illegal_customer";
        }
        return REDIRECT_PATH;
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        logger.debug("Editing customer, id: {}", id);
        model.addAttribute("customer", customerService.get(id));
        return "customers/edit";
    }

    @PostMapping("/{id}/edit")
    public String update(Model model, @ModelAttribute("customer") CustomerDTO customerDTO,
                         @PathVariable("id") int id) {
        logger.debug("Updating customer, id: {}", id);
        try {
            customerService.updateName(id, customerDTO.getName());
        } catch (ServiceException e) {
            model.addAttribute("customer", customerDTO.getName());
            return "customers/illegal_customer";
        }
        return REDIRECT_PATH;
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        logger.debug("Deleting customer, id: {}", id);
        try {
            customerService.delete(id);
        } catch (IllegalDeleteException e) {
            return "customers/illegal_delete";
        }
        return REDIRECT_PATH;
    }
}
