package ru.kitaev.factory.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.dto.PurchaseOrderDTO;
import ru.kitaev.factory.service.CustomerService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.PurchaseOrderService;
import ru.kitaev.factory.service.ServiceException;

@Controller
@RequestMapping("/purchase_orders")
public class PurchaseOrdersController {
    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrdersController.class.getName());
    private final PurchaseOrderService purchaseOrderService;
    private final CustomerService customerService;

    @Autowired
    public PurchaseOrdersController(PurchaseOrderService purchaseOrderService, CustomerService customerService) {
        this.purchaseOrderService = purchaseOrderService;
        this.customerService = customerService;
    }

    @GetMapping()
    public String showAll(Model model) {
        logger.debug("Showing all purchase orders");
        model.addAttribute("purchase_orders", purchaseOrderService.getAll());
        return "purchase_orders/all";
    }

    @GetMapping("/new")
    public String addNew(Model model) {
        logger.debug("Adding new purchase order");
        model.addAttribute("purchase_order", new PurchaseOrderDTO());
        model.addAttribute("customers", customerService.getAll());
        return "purchase_orders/new";
    }

    @PostMapping()
    public String create(Model model, @ModelAttribute("purchase_order") PurchaseOrderDTO purchaseOrderDTO) {
        logger.debug("Creating new purchase order");
        logger.trace("Purchase order received from front: {}", purchaseOrderDTO);
        CustomerDTO customerDTO = customerService.get(purchaseOrderDTO.getCustomer().getId());
        purchaseOrderDTO.setCustomer(customerDTO);
        try {
            purchaseOrderService.save(purchaseOrderDTO);
        } catch (ServiceException e) {
            model.addAttribute("customer", customerDTO.getName());
            model.addAttribute("number", purchaseOrderDTO.getNumber());
            return "purchase_orders/illegal_purchase_order";
        }
        return "redirect:/purchase_orders";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        logger.debug("Editing purchase order, id: {}", id);
        model.addAttribute("purchase_order", purchaseOrderService.get(id));
        model.addAttribute("customers", customerService.getAll());
        return "purchase_orders/edit";
    }

    @PostMapping("/{id}/edit")
    public String update(Model model, @ModelAttribute("purchase_order") PurchaseOrderDTO purchaseOrderDTO,
                         @PathVariable("id") int id) {
        logger.debug("Updating purchase order, id: {}", id);
        CustomerDTO customerDTO = customerService.get(purchaseOrderDTO.getCustomer().getId());
        purchaseOrderDTO.setCustomer(customerDTO);
        try {
            purchaseOrderService.update(purchaseOrderDTO);
        } catch (ServiceException e) {
            model.addAttribute("customer", customerDTO.getName());
            model.addAttribute("number", purchaseOrderDTO.getNumber());
            return "purchase_orders/illegal_purchase_order";
        }
        return "redirect:/purchase_orders";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        logger.debug("Deleting purchase order, id: {}", id);
        try {
            purchaseOrderService.delete(id);
        } catch (IllegalDeleteException e) {
            return "purchase_orders/illegal_delete";
        }
        return "redirect:/purchase_orders";
    }
}
