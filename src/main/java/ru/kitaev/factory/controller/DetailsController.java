package ru.kitaev.factory.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kitaev.factory.dto.*;
import ru.kitaev.factory.service.*;

@Controller
@RequestMapping("/details")
public class DetailsController {
    private static final Logger logger = LoggerFactory.getLogger(DetailsController.class.getName());
    private final DetailService detailService;
    private final DrawingService drawingService;
    private final CustomerService customerService;
    private final PurchaseOrderService purchaseOrderService;
    private final StateService stateService;

    @Autowired
    public DetailsController(DetailService detailService, DrawingService drawingService,
                             CustomerService customerService, PurchaseOrderService purchaseOrderService,
                             StateService stateService) {
        this.detailService = detailService;
        this.drawingService = drawingService;
        this.customerService = customerService;
        this.purchaseOrderService = purchaseOrderService;
        this.stateService = stateService;
    }

    @GetMapping()
    public String showAll(Model model) {
        logger.debug("Showing all details");
        model.addAttribute("details", detailService.getAll());
        return "details/all";
    }

    @GetMapping("/new")
    public String addNew(Model model) {
        logger.debug("Adding new detail");
        model.addAttribute("detail", new DetailDTO());
        model.addAttribute("drawings", drawingService.getAll());
        model.addAttribute("purchaseOrders", purchaseOrderService.getAll());
        return "details/new";
    }

    @PostMapping()
    public String create(Model model, @ModelAttribute("detail") DetailDTO detailDTO) {
        logger.debug("Creating new detail");
        logger.trace("Detail received from front: {}", detailDTO);
        DrawingDTO drawingDTO = drawingService.get(detailDTO.getDrawing().getId());
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderService.get(detailDTO.getPurchaseOrder().getId());
        StateDTO stateDTO = stateService.getByName("initial");
        detailDTO.setDrawing(drawingDTO);
        detailDTO.setPurchaseOrder(purchaseOrderDTO);
        detailDTO.setState(stateDTO);
        try {
            detailService.save(detailDTO);
        } catch (ServiceException e) {
            model.addAttribute("detail", detailDTO.getNumber());
            return "details/illegal_detail";
        }
        return "redirect:/details";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        logger.debug("Editing detail, id: {}", id);
        DetailDTO detailDTO = detailService.get(id);
        CustomerDTO customerDTO = customerService.get(detailDTO.getPurchaseOrder().getCustomer().getId());
        model.addAttribute("detail", detailDTO);
        model.addAttribute("drawings", drawingService.getAll());
        model.addAttribute("purchaseOrders", customerDTO.getPurchaseOrderList());
        model.addAttribute("states", stateService.getAll());
        return "details/edit";
    }

    @PostMapping("/{id}/edit")
    public String update(Model model, @ModelAttribute("detail") DetailDTO detailDTO,
                         @PathVariable("id") int id) {
        logger.debug("Updating detail, id: {}", id);
        DrawingDTO drawingDTO = drawingService.get(detailDTO.getDrawing().getId());
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderService.get(detailDTO.getPurchaseOrder().getId());
        StateDTO stateDTO = stateService.getById(detailDTO.getState().getId());
        detailDTO.setDrawing(drawingDTO);
        detailDTO.setPurchaseOrder(purchaseOrderDTO);
        detailDTO.setState(stateDTO);
        try {
            detailService.update(detailDTO);
        } catch (ServiceException e) {
            model.addAttribute("detail", detailDTO.getNumber());
            return "details/illegal_detail";
        }
        return "redirect:/details";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        logger.debug("Deleting detail, id: {}", id);
        detailService.delete(id);
        return "redirect:/details";
    }
}
