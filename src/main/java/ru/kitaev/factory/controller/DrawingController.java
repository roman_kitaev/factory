package ru.kitaev.factory.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kitaev.factory.dto.DrawingDTO;
import ru.kitaev.factory.service.DrawingService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.ServiceException;

@Controller
@RequestMapping("/drawings")
public class DrawingController {
    private static final Logger logger = LoggerFactory.getLogger(DrawingController.class.getName());
    private final DrawingService drawingService;

    @Autowired
    public DrawingController(DrawingService drawingService) {
        this.drawingService = drawingService;
    }

    @GetMapping()
    public String showAll(Model model) {
        logger.debug("Showing all drawings");
        model.addAttribute("drawings", drawingService.getAll());
        return "drawings/all";
    }

    @GetMapping("/new")
    public String addNew(Model model) {
        logger.debug("Adding new drawing");
        model.addAttribute("drawing", new DrawingDTO());
        return "drawings/new";
    }

    @PostMapping()
    public String create(Model model, @ModelAttribute("drawing") DrawingDTO drawingDTO) {
        logger.debug("Creating new drawing");
        logger.trace("Drawing received from front: {}", drawingDTO);
        try {
            drawingService.save(drawingDTO);
        } catch (ServiceException e) {
            model.addAttribute("drawing", drawingDTO.getNumber());
            return "drawings/illegal_drawing";
        }
        return "redirect:/drawings";
    }

    @GetMapping("/{id}/edit")
    public String edit(Model model, @PathVariable("id") int id) {
        logger.debug("Editing drawing, id: {}", id);
        model.addAttribute("drawing", drawingService.get(id));
        return "drawings/edit";
    }

    @PostMapping("/{id}/edit")
    public String update(Model model, @ModelAttribute("drawing") DrawingDTO drawingDTO,
                         @PathVariable("id") int id) {
        logger.debug("Updating drawing, id: {}", id);
        try {
            drawingService.updateNumber(id, drawingDTO.getNumber());
        } catch (ServiceException e) {
            model.addAttribute("drawing", drawingDTO.getNumber());
            return "drawings/illegal_drawing";
        }
        return "redirect:/drawings";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        logger.debug("Deleting drawing, id: {}", id);
        try {
            drawingService.delete(id);
        } catch (IllegalDeleteException e) {
            return "drawings/illegal_delete";
        }
        return "redirect:/drawings";
    }
}
