package ru.kitaev.factory.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.kitaev.factory.domain.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class DetailRepositoryTest {
    @Autowired
    private DetailRepository detailRepository;

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private DrawingRepository drawingRepository;

    @Autowired
    private StateRepository stateRepository;

    @Test
    void insert_and_getById() {
        Customer customer = new Customer("Customer1");
        customerRepository.save(customer);

        PurchaseOrder purchaseOrder = new PurchaseOrder("Order1", customerRepository.getById(customer.getId()));
        purchaseOrderRepository.save(purchaseOrder);

        Drawing drawing = new Drawing("Drawing1");
        drawingRepository.save(drawing);

        State state = new State("STATE 1");
        stateRepository.save(state);

        Detail expectedDetail = new Detail("Detail1", drawing, purchaseOrder, state);
        detailRepository.save(expectedDetail);

        Detail actualDetail = detailRepository.findById(expectedDetail.getId()).orElseThrow();

        Assertions.assertEquals(expectedDetail, actualDetail);
        Assertions.assertEquals(expectedDetail.getNumber(), actualDetail.getNumber());
        Assertions.assertEquals(expectedDetail.getPurchaseOrder(), actualDetail.getPurchaseOrder());
        Assertions.assertEquals(expectedDetail.getDrawing(), actualDetail.getDrawing());
        Assertions.assertEquals(expectedDetail.getState(), actualDetail.getState());
    }
}