package ru.kitaev.factory.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.kitaev.factory.domain.State;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class StateRepositoryTest {
    @Autowired
    private StateRepository stateRepository;

    @Test
    void insert_and_getByName() {
        State expectedState = new State("STATE 1");
        stateRepository.save(expectedState);

        State actualState = stateRepository.findByName("STATE 1");

        assertEquals(expectedState, actualState);
        assertEquals(expectedState.getName(), actualState.getName());
    }
}