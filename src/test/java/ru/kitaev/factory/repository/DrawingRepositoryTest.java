package ru.kitaev.factory.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.kitaev.factory.domain.Drawing;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class DrawingRepositoryTest {
    @Autowired
    private DrawingRepository drawingRepository;

    @Test
    void insert_and_getById() {
        Drawing expectedDrawing = new Drawing("Drawing1");
        drawingRepository.save(expectedDrawing);

        Drawing actualDrawing = drawingRepository.getById(expectedDrawing.getId());

        assertEquals(expectedDrawing, actualDrawing);
        assertEquals(expectedDrawing.getNumber(), actualDrawing.getNumber());
    }
}