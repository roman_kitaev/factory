package ru.kitaev.factory.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.kitaev.factory.domain.Customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class CustomerRepositoryTest {
    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void insert_and_getById() {
        Customer expectedCustomer = new Customer("Customer 1");
        customerRepository.save(expectedCustomer);

        Customer actualCustomer = customerRepository.getById(expectedCustomer.getId());

        assertEquals(expectedCustomer, actualCustomer);
    }
}