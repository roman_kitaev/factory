package ru.kitaev.factory.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.domain.PurchaseOrder;

import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderRepositoryTest {
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    void insert_and_getById() {
        Customer customer = new Customer("Customer1");
        customerRepository.save(customer);

        PurchaseOrder expectedPurchaseOrder = new PurchaseOrder("Order1", customerRepository.getById(customer.getId()));
        purchaseOrderRepository.save(expectedPurchaseOrder);

        PurchaseOrder actualPurchaseOrder = purchaseOrderRepository.getById(expectedPurchaseOrder.getId());

        Assertions.assertEquals(expectedPurchaseOrder, actualPurchaseOrder);
        Assertions.assertEquals(expectedPurchaseOrder.getNumber(), actualPurchaseOrder.getNumber());
        Assertions.assertEquals(expectedPurchaseOrder.getCustomer(), actualPurchaseOrder.getCustomer());
    }

    @Test
    void countByCustomer() {
        Customer customer = new Customer("Customer1");
        customerRepository.save(customer);

        purchaseOrderRepository.save(new PurchaseOrder("Order1", customerRepository.getById(customer.getId())));
        purchaseOrderRepository.save(new PurchaseOrder("Order2", customerRepository.getById(customer.getId())));
        purchaseOrderRepository.save(new PurchaseOrder("Order3", customerRepository.getById(customer.getId())));

        Assertions.assertEquals(3, purchaseOrderRepository.countByCustomer(customer));
    }

    @Test
    void findByCustomer() {
        Customer customer1 = new Customer("Customer1");
        customerRepository.save(customer1);

        Customer customer2 = new Customer("Customer2");
        customerRepository.save(customer2);

        purchaseOrderRepository.saveAll(Arrays.asList(
            new PurchaseOrder("Order1", customer1),
            new PurchaseOrder("Order2", customer1),
            new PurchaseOrder("Order3", customer1),
            new PurchaseOrder("Order4", customer2)));

        Assertions.assertEquals(3, purchaseOrderRepository.findByCustomer(customer1).size());
    }
}