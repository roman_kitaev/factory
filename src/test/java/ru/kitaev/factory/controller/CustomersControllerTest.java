package ru.kitaev.factory.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.service.CustomerService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.ServiceException;

import java.util.Arrays;

@WebMvcTest(CustomersController.class)
class CustomersControllerTest {
    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void showAll() throws Exception {
        Mockito.when(customerService.getAll()).thenReturn(Arrays.asList(
            new CustomerDTO(10, "TestCustomer 1"),
            new CustomerDTO(20, "TestCustomer 2")));

        String generatedHtml = mockMvc.
            perform(MockMvcRequestBuilders.get("/customers")).
            andReturn().
            getResponse().
            getContentAsString();

        Assertions.assertTrue(generatedHtml.contains("TestCustomer 1"));
        Assertions.assertTrue(generatedHtml.contains("TestCustomer 2"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/customers/10/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/customers/20/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/customers/10/edit\" method=\"GET\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/customers/20/edit\" method=\"GET\">"));
    }

    @Test
    void whenGetAddNew_thenReturnsCorrectViewName() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.get("/customers/new")).
            andExpect(MockMvcResultMatchers.view().name("customers/new"));
    }

    @Test
    void create() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers").
                param("name", customerDTO.getName()));

        Mockito.verify(customerService, Mockito.times(1)).
            save(new CustomerDTO(customerDTO.getName()));
    }

    @Test
    void whenCreateWithUsedName_thenReturnsCorrectViewName() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO("Customer 1");
        Mockito.doThrow(new ServiceException("Customer with name " +
            customerDTO.getName() + " already exists")).when(customerService).save(customerDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers").
                param("name", customerDTO.getName())).
            andExpect(MockMvcResultMatchers.view().name("customers/illegal_customer"));
    }

    @Test
    void whenGetEdit_thenReturnsCorrectViewName() throws Exception {
        Mockito.when(customerService.get(1)).thenReturn(
            new CustomerDTO(1, "Customer"));

        mockMvc.
            perform(MockMvcRequestBuilders.get("/customers/1/edit")).
            andExpect(MockMvcResultMatchers.view().name("customers/edit"));
    }

    @Test
    void update() throws Exception {
        String newCustomerName = "NewCustomerName";

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers/1/edit").
                param("name", newCustomerName));

        Mockito.verify(customerService, Mockito.times(1)).
            updateName(1, newCustomerName);
    }

    @Test
    void whenUpdateWithUsedName_thenReturnsCorrectViewName() throws Exception {
        String newCustomerName = "NewCustomerName";
        Mockito.doThrow(new ServiceException()).when(customerService).updateName(1, newCustomerName);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers/1/edit").
                param("name", newCustomerName)).
            andExpect(MockMvcResultMatchers.view().name("customers/illegal_customer"));
    }

    @Test
    void delete() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers/1/delete"));

        Mockito.verify(customerService, Mockito.times(1)).delete(1);
    }

    @Test
    void illegal_delete() throws Exception {
        int id = 1;
        Mockito.doThrow(new IllegalDeleteException("Can not delete the customer - " +
                "you have to delete purchase orders for it first, customer id = " + id)).
            when(customerService).delete(id);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/customers/1/delete")).
            andExpect(MockMvcResultMatchers.view().name("customers/illegal_delete"));
    }
}