package ru.kitaev.factory.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.dto.PurchaseOrderDTO;
import ru.kitaev.factory.service.CustomerService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.PurchaseOrderService;
import ru.kitaev.factory.service.ServiceException;

import java.util.Arrays;

@WebMvcTest(PurchaseOrdersController.class)
class PurchaseOrdersControllerTest {
    @MockBean
    private PurchaseOrderService purchaseOrderService;
    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void showAll() throws Exception {
        Mockito.when(purchaseOrderService.getAll()).thenReturn(Arrays.asList(
            new PurchaseOrderDTO(10, "Test PO 1", new CustomerDTO(10, "TestCustomer 1")),
            new PurchaseOrderDTO(20, "Test PO 2", new CustomerDTO(20, "TestCustomer 2"))));

        String generatedHtml = mockMvc.
            perform(MockMvcRequestBuilders.get("/purchase_orders")).
            andReturn().
            getResponse().
            getContentAsString();

        Assertions.assertTrue(generatedHtml.contains("Test PO 1"));
        Assertions.assertTrue(generatedHtml.contains("Test PO 2"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/purchase_orders/10/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/purchase_orders/20/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/purchase_orders/10/edit\" method=\"GET\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/purchase_orders/20/edit\" method=\"GET\">"));
        Assertions.assertTrue(generatedHtml.contains("TestCustomer 1"));
        Assertions.assertTrue(generatedHtml.contains("TestCustomer 2"));
    }

    @Test
    void whenGetAddNew_thenReturnsCorrectViewName() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.get("/purchase_orders/new")).
            andExpect(MockMvcResultMatchers.view().name("purchase_orders/new"));
    }

    @Test
    void create() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO("Order 1", customerDTO);
        Mockito.when(customerService.get(1)).thenReturn(customerDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders").
                param("number", "Order 1").
                param("customer.id", "1"));

        Mockito.verify(purchaseOrderService, Mockito.times(1)).
            save(purchaseOrderDTO);
    }

    @Test
    void whenCreateWithUsedNumber_thenReturnsCorrectViewName() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO("Order 1", customerDTO);
        Mockito.when(customerService.get(1)).thenReturn(customerDTO);
        Mockito.doThrow(new ServiceException()).when(purchaseOrderService).save(purchaseOrderDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders").
                param("number", "Order 1").
                param("customer.id", "1")).
            andExpect(MockMvcResultMatchers.view().name("purchase_orders/illegal_purchase_order"));
    }

    @Test
    void whenGetEdit_thenReturnsCorrectViewName() throws Exception {
        Mockito.when(purchaseOrderService.get(1)).thenReturn(
            new PurchaseOrderDTO(1, "Order 1", new CustomerDTO(1, "Customer")));

        mockMvc.
            perform(MockMvcRequestBuilders.get("/purchase_orders/1/edit")).
            andExpect(MockMvcResultMatchers.view().name("purchase_orders/edit"));
    }

    @Test
    void update() throws Exception {
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");
        PurchaseOrderDTO purchaseOrderDTOToUpdate =
            new PurchaseOrderDTO(1, "Order 2", newCustomerDTO);
        Mockito.when(customerService.get(2)).thenReturn(newCustomerDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders/1/edit").
                param("id", "1").
                param("number", "Order 2").
                param("customer.id", "2").
                param("customer.name", "Customer 2"));

        Mockito.verify(purchaseOrderService, Mockito.times(1)).
            update(purchaseOrderDTOToUpdate);
    }

    @Test
    void whenUpdateWithUsedPairOfNumberAndCustomer_thenReturnsCorrectViewName() throws Exception {
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");
        PurchaseOrderDTO purchaseOrderDTOToUpdate = new PurchaseOrderDTO(1, "Order 2", newCustomerDTO);
        Mockito.when(customerService.get(2)).thenReturn(newCustomerDTO);
        Mockito.doThrow(new ServiceException()).when(purchaseOrderService).update(purchaseOrderDTOToUpdate);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders/1/edit").
                param("id", "1").
                param("number", "Order 2").
                param("customer.id", "2").
                param("customer.name", "Customer 2")).
            andExpect(MockMvcResultMatchers.view().name("purchase_orders/illegal_purchase_order"));
    }

    @Test
    void delete() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders/1/delete"));

        Mockito.verify(purchaseOrderService, Mockito.times(1)).delete(1);
    }

    @Test
    void illegal_delete() throws Exception {
        int id = 1;
        Mockito.doThrow(new IllegalDeleteException("Can not delete the purchase order - " +
                "you have to delete details for it first, purchase order id = " + id)).
            when(purchaseOrderService).delete(id);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/purchase_orders/1/delete")).
            andExpect(MockMvcResultMatchers.view().name("purchase_orders/illegal_delete"));
    }
}