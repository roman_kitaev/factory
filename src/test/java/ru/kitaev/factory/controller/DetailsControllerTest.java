package ru.kitaev.factory.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.kitaev.factory.dto.*;
import ru.kitaev.factory.service.*;

import java.util.Arrays;

@WebMvcTest(DetailsController.class)
class DetailsControllerTest {
    @MockBean
    private DetailService detailService;
    @MockBean
    private DrawingService drawingService;
    @MockBean
    private PurchaseOrderService purchaseOrderService;
    @MockBean
    private CustomerService customerService;
    @MockBean
    private StateService stateService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void showAll() throws Exception {
        Mockito.when(detailService.getAll()).thenReturn(Arrays.asList(
            new DetailDTO(100, "Det 1",
                new DrawingDTO(1, "Drawing 1"),
                new PurchaseOrderDTO(1, "Order 1",
                    new CustomerDTO(1, "Customer 1")),
                new StateDTO(1, "STATE 1")),
            new DetailDTO(201, "Det 2",
                new DrawingDTO(2, "Drawing 2"),
                new PurchaseOrderDTO(2, "Order 2",
                    new CustomerDTO(1, "Customer 1")),
                new StateDTO(2, "STATE 2"))
        ));

        String generatedHtml = mockMvc.
            perform(MockMvcRequestBuilders.get("/details")).
            andReturn().
            getResponse().
            getContentAsString();

        Assertions.assertTrue(generatedHtml.contains("Det 1"));
        Assertions.assertTrue(generatedHtml.contains("Det 2"));
        Assertions.assertTrue(generatedHtml.contains("Drawing 1"));
        Assertions.assertTrue(generatedHtml.contains("Drawing 2"));
        Assertions.assertTrue(generatedHtml.contains("Order 1"));
        Assertions.assertTrue(generatedHtml.contains("Order 2"));
        Assertions.assertTrue(generatedHtml.contains("Customer 1"));
        Assertions.assertTrue(generatedHtml.contains("STATE 1"));
        Assertions.assertTrue(generatedHtml.contains("STATE 2"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/details/100/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/details/201/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/details/100/edit\" method=\"GET\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/details/201/edit\" method=\"GET\">"));
    }

    @Test
    void whenGetAddNew_thenReturnsCorrectViewName() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.get("/details/new")).
            andExpect(MockMvcResultMatchers.view().name("details/new"));
    }

    @Test
    void create() throws Exception {
        String detailNumber = "Detail 1";
        DrawingDTO drawingDTO = new DrawingDTO(1, "Drawing 1");
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1", customerDTO);
        StateDTO stateDTO = new StateDTO(1, "initial");

        DetailDTO detailDTO = new DetailDTO(detailNumber, drawingDTO, purchaseOrderDTO, stateDTO);

        Mockito.when(drawingService.get(1)).thenReturn(drawingDTO);
        Mockito.when(purchaseOrderService.get(1)).thenReturn(purchaseOrderDTO);
        Mockito.when(stateService.getByName("initial")).thenReturn(stateDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/details").
                param("number", detailNumber).
                param("drawing.id", "1").
                param("purchaseOrder.id", "1"));

        Mockito.verify(detailService, Mockito.times(1)).save(detailDTO);
    }

    @Test
    void whenCreateWithUsedNumber_thenReturnsCorrectViewName() throws Exception {
        DrawingDTO drawingDTO = new DrawingDTO(1, "Drawing 1");
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1",
            new CustomerDTO(1, "Customer 1"));
        StateDTO stateDTO = new StateDTO(1, "STATE 1");
        DetailDTO detailDTO = new DetailDTO("Detail 1",
            drawingDTO, purchaseOrderDTO, stateDTO);

        Mockito.when(drawingService.get(1)).thenReturn(drawingDTO);
        Mockito.when(purchaseOrderService.get(1)).thenReturn(purchaseOrderDTO);
        Mockito.when(stateService.getByName("initial")).thenReturn(stateDTO);
        Mockito.doThrow(new ServiceException()).when(detailService).save(detailDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/details").
                param("number", "Detail 1").
                param("drawing.id", "1").
                param("purchaseOrder.id", "1").
                param("state.id", "1")).
            andExpect(MockMvcResultMatchers.view().name("details/illegal_detail"));
    }

    @Test
    void whenGetEdit_thenReturnsCorrectViewName() throws Exception {
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");

        Mockito.when(detailService.get(1)).thenReturn(new DetailDTO(1, "Det 1",
            new DrawingDTO(1, "Drawing 1"),
            new PurchaseOrderDTO(1, "Order 1", customerDTO),
            new StateDTO(1, "STATE 1")));
        Mockito.when(customerService.get(1)).thenReturn(customerDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.get("/details/1/edit")).
            andExpect(MockMvcResultMatchers.view().name("details/edit"));
    }

    @Test
    void update() throws Exception {
        DrawingDTO newDrawingDTO = new DrawingDTO(2, "Drawing 2");
        PurchaseOrderDTO newPurchaseOrderDTO = new PurchaseOrderDTO(2, "order 2",
            new CustomerDTO(2, "Customer 2"));
        StateDTO newStateDTO = new StateDTO(2, "State 2");

        Mockito.when(drawingService.get(2)).thenReturn(newDrawingDTO);
        Mockito.when(purchaseOrderService.get(2)).thenReturn(newPurchaseOrderDTO);
        Mockito.when(stateService.getById(2)).thenReturn(newStateDTO);

        DetailDTO detailDTOToUpdate = new DetailDTO(1, "Detail 2",
            newDrawingDTO, newPurchaseOrderDTO, newStateDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/details/1/edit").
                param("id", "1").
                param("number", "Detail 2").
                param("drawing.id", "2").
                param("purchaseOrder.id", "2").
                param("state.id", "2"));

        Mockito.verify(detailService, Mockito.times(1)).update(detailDTOToUpdate);
    }

    @Test
    void whenUpdateWithUsedNumber_thenReturnsCorrectViewName() throws Exception {
        DrawingDTO newDrawingDTO = new DrawingDTO(2, "Drawing 2");
        PurchaseOrderDTO newPurchaseOrderDTO = new PurchaseOrderDTO(2, "order 2",
            new CustomerDTO(2, "Customer 2"));
        StateDTO newStateDTO = new StateDTO(2, "State 2");
        DetailDTO detailDTOToUpdate = new DetailDTO(1, "Detail 2",
            newDrawingDTO, newPurchaseOrderDTO, newStateDTO);

        Mockito.when(drawingService.get(2)).thenReturn(newDrawingDTO);
        Mockito.when(purchaseOrderService.get(2)).thenReturn(newPurchaseOrderDTO);
        Mockito.when(stateService.getById(2)).thenReturn(newStateDTO);
        Mockito.doThrow(new ServiceException()).when(detailService).update(detailDTOToUpdate);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/details/1/edit").
                param("id", "1").
                param("number", "Detail 2").
                param("drawing.id", "2").
                param("purchaseOrder.id", "2").
                param("state.id", "2")).
            andExpect(MockMvcResultMatchers.view().name("details/illegal_detail"));
    }

    @Test
    void delete() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/details/1/delete"));

        Mockito.verify(detailService, Mockito.times(1)).delete(1);
    }
}