package ru.kitaev.factory.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.kitaev.factory.dto.DrawingDTO;
import ru.kitaev.factory.service.DrawingService;
import ru.kitaev.factory.service.IllegalDeleteException;
import ru.kitaev.factory.service.ServiceException;

import java.util.Arrays;

@WebMvcTest(DrawingController.class)
class DrawingControllerTest {
    @MockBean
    private DrawingService drawingService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void showAll() throws Exception {
        Mockito.when(drawingService.getAll()).thenReturn(Arrays.asList(
            new DrawingDTO(11, "Drawing 1"),
            new DrawingDTO(22, "Drawing 2")));

        String generatedHtml = mockMvc.
            perform(MockMvcRequestBuilders.get("/drawings")).
            andReturn().
            getResponse().
            getContentAsString();

        Assertions.assertTrue(generatedHtml.contains("Drawing 1"));
        Assertions.assertTrue(generatedHtml.contains("Drawing 2"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/drawings/11/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/drawings/22/delete\" method=\"POST\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/drawings/11/edit\" method=\"GET\">"));
        Assertions.assertTrue(generatedHtml.contains("<form action=\"/drawings/22/edit\" method=\"GET\">"));
    }

    @Test
    void whenGetAddNew_thenReturnsCorrectViewName() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.get("/drawings/new")).
            andExpect(MockMvcResultMatchers.view().name("drawings/new"));
    }

    @Test
    void create() throws Exception {
        DrawingDTO drawingDTO = new DrawingDTO(1, "Drawing 1");

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings").
                param("number", drawingDTO.getNumber()));

        Mockito.verify(drawingService, Mockito.times(1)).
            save(new DrawingDTO(drawingDTO.getNumber()));
    }

    @Test
    void whenCreateWithUsedNumber_thenReturnsCorrectViewName() throws Exception {
        DrawingDTO drawingDTO = new DrawingDTO("Drawing 1");
        Mockito.doThrow(new ServiceException()).when(drawingService).save(drawingDTO);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings").
                param("number", drawingDTO.getNumber())).
            andExpect(MockMvcResultMatchers.view().name("drawings/illegal_drawing"));
    }

    @Test
    void whenGetEdit_thenReturnsCorrectViewName() throws Exception {
        Mockito.when(drawingService.get(1)).thenReturn(
            new DrawingDTO(1, "Drawing 1"));

        mockMvc.
            perform(MockMvcRequestBuilders.get("/drawings/1/edit")).
            andExpect(MockMvcResultMatchers.view().name("drawings/edit"));
    }

    @Test
    void update() throws Exception {
        String newDrawingNumber = "NewDrawingNumber";

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings/1/edit").
                param("number", newDrawingNumber));

        Mockito.verify(drawingService, Mockito.times(1)).
            updateNumber(1, newDrawingNumber);
    }

    @Test
    void whenUpdateNumberWithUsedNumber_thenReturnsCorrectViewName() throws Exception {
        String newDrawingNumber = "NewDrawingNumber";
        Mockito.doThrow(new ServiceException()).when(drawingService).updateNumber(1, newDrawingNumber);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings/1/edit").
                param("number", newDrawingNumber)).
            andExpect(MockMvcResultMatchers.view().name("drawings/illegal_drawing"));
    }

    @Test
    void delete() throws Exception {
        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings/1/delete"));

        Mockito.verify(drawingService, Mockito.times(1)).delete(1);
    }

    @Test
    void illegal_delete() throws Exception {
        int id = 1;
        Mockito.doThrow(new IllegalDeleteException("Can not delete the drawing - " +
                "you have to delete details with it first, drawing id = " + id)).
            when(drawingService).delete(id);

        mockMvc.
            perform(MockMvcRequestBuilders.
                post("/drawings/1/delete")).
            andExpect(MockMvcResultMatchers.view().name("drawings/illegal_delete"));
    }
}