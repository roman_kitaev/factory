package ru.kitaev.factory.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import ru.kitaev.factory.domain.Drawing;
import ru.kitaev.factory.dto.DrawingDTO;
import ru.kitaev.factory.repository.DrawingRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class DrawingServiceTest {
    private DrawingService drawingService;
    @Mock
    private DrawingRepository drawingRepository;
    @Mock
    private DetailService detailService;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        drawingService = new DrawingService(drawingRepository, detailService, modelMapper);
    }

    @Test
    void save() {
        Drawing drawing = new Drawing(1, "Drawing 1");
        DrawingDTO drawingDTO = new DrawingDTO(drawing.getId(), drawing.getNumber());

        Mockito.when(modelMapper.map(drawingDTO, Drawing.class)).thenReturn(drawing);

        drawingService.save(drawingDTO);
        Mockito.verify(drawingRepository, Mockito.times(1)).save(drawing);
    }

    @Test
    void save_throwsCorrectExceptionIfNameIsUsed() {
        DrawingDTO drawingDTO = new DrawingDTO(1, "Drawing 1");
        Mockito.when(drawingRepository.findByNumber(drawingDTO.getNumber())).
            thenReturn(new Drawing(2, "Drawing 1"));

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> drawingService.save(drawingDTO));
        Assertions.assertEquals("Drawing with number " + drawingDTO.getNumber() + " already exists",
            throwable.getMessage());
    }

    @Test
    void get() {
        Drawing drawing = new Drawing(1, "Drawing 1");
        Mockito.when(drawingRepository.findById(1)).thenReturn(Optional.of(drawing));

        drawingService.get(1);

        Mockito.verify(drawingRepository, Mockito.times(1)).findById(1);
        Mockito.verify(modelMapper, Mockito.times(1)).map(drawing, DrawingDTO.class);
    }

    @Test
    void getAll() {
        List<Drawing> drawingList = Arrays.asList(
            new Drawing(1, "Drawing 1"), new Drawing(2, "Drawing 2"));
        Mockito.when(drawingRepository.findAll()).thenReturn(drawingList);
        Mockito.when(modelMapper.map(drawingList.get(0), DrawingDTO.class)).
            thenReturn(new DrawingDTO(1, "Drawing 1"));
        Mockito.when(modelMapper.map(drawingList.get(1), DrawingDTO.class)).
            thenReturn(new DrawingDTO(2, "Drawing 2"));

        List<DrawingDTO> actualDrawingDTOs = drawingService.getAll();

        Mockito.verify(drawingRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(new DrawingDTO(1, "Drawing 1"), actualDrawingDTOs.get(0));
        Assertions.assertEquals(new DrawingDTO(2, "Drawing 2"), actualDrawingDTOs.get(1));
    }

    @Test
    void ServiceException_ifEntityDoesNotExist() {
        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> drawingService.get(1000));
        Assertions.assertEquals("Could not find a drawing, id 1000", throwable.getMessage());
    }

    @Test
    void updateNumber() {
        Drawing drawing = new Drawing(1, "Number1");

        Mockito.when(drawingRepository.findById(1)).thenReturn(Optional.of(drawing));

        drawingService.updateNumber(1, "Number 2");
        Mockito.verify(drawingRepository, Mockito.times(1)).findById(1);
        Mockito.verify(drawingRepository, Mockito.times(1)).save(
            new Drawing(1, "Number 2")); //entities are compared only by id
    }

    @Test
    void updateNumber_doesNothingIfNumberIsTheSame() {
        Drawing drawing = new Drawing(1, "Number 1");

        Mockito.when(drawingRepository.findById(1)).thenReturn(Optional.of(drawing));

        drawingService.updateNumber(1, "Number 1");
        Mockito.verify(drawingRepository, Mockito.times(1)).findById(1);
        Mockito.verify(drawingRepository, Mockito.never()).save(Mockito.any(Drawing.class));
    }

    @Test
    void updateNumber_throwsCorrectExceptionIfNameIsUsed() {
        String newNumber = "Number 2";
        Mockito.when(drawingRepository.findById(1)).thenReturn(Optional.of(new Drawing(1, "Drawing 1")));
        Mockito.when(drawingRepository.findByNumber(newNumber)).thenReturn(new Drawing());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> drawingService.
            updateNumber(1, newNumber));
        Assertions.assertEquals("Drawing with number " + newNumber + " already exists",
            throwable.getMessage());
    }

    @Test
    void delete() {
        int id = 1;
        Drawing drawing = new Drawing(id, "Drawing 1");
        Mockito.when(drawingRepository.findById(id)).thenReturn(Optional.of(drawing));
        Mockito.when(detailService.countByDrawing(drawing)).thenReturn(0L);

        drawingService.delete(id);
        Mockito.verify(drawingRepository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void deleteThrowsException_ifDrawingNotFound() {
        int id = 1;
        Mockito.when(drawingRepository.findById(id)).thenReturn(Optional.empty());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> drawingService.delete(id));
        Assertions.assertEquals("Could not find a drawing to delete, id = " + id, throwable.getMessage());
    }

    @Test
    void deleteThrowsException_ifThereAreDetailsWithLinksToDrawing() {
        int id = 1;
        Drawing drawing = new Drawing(id, "Drawing 1");
        Mockito.when(drawingRepository.findById(id)).thenReturn(Optional.of(drawing));
        Mockito.when(detailService.countByDrawing(drawing)).thenReturn(1L);

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> drawingService.delete(id));
        Assertions.assertEquals("Can not delete the drawing - " +
            "you have to delete details with it first, drawing id = " + id, throwable.getMessage());
    }
}