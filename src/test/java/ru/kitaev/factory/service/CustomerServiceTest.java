package ru.kitaev.factory.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.repository.CustomerRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {
    private CustomerService customerService;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private PurchaseOrderService purchaseOrderService;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        customerService = new CustomerService(customerRepository, purchaseOrderService,
            modelMapper);
    }

    @Test
    void save() {
        Customer customer = new Customer(1, "Customer 1");
        CustomerDTO customerDTO = new CustomerDTO(customer.getId(), customer.getName());

        Mockito.when(modelMapper.map(customerDTO, Customer.class)).thenReturn(customer);

        customerService.save(customerDTO);
        Mockito.verify(customerRepository, Mockito.times(1)).save(customer);
    }

    @Test
    void save_throwsCorrectExceptionIfNameIsUsed() {
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        Mockito.when(customerRepository.findByName(customerDTO.getName())).
            thenReturn(new Customer(2, "Customer 1"));

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> customerService.save(customerDTO));
        Assertions.assertEquals("Customer with name " + customerDTO.getName() + " already exists",
            throwable.getMessage());
    }

    @Test
    void get() {
        Customer customer = new Customer(1, "Customer 1");
        Mockito.when(customerRepository.findWithOrdersById(1)).thenReturn(customer);

        customerService.get(1);

        Mockito.verify(customerRepository, Mockito.times(1)).findWithOrdersById(1);
        Mockito.verify(modelMapper, Mockito.times(1)).map(customer, CustomerDTO.class);
    }

    @Test
    void getAll() {
        List<Customer> customerList = Arrays.asList(
            new Customer(1, "Customer 1"), new Customer(2, "Customer 2"));
        Mockito.when(customerRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).
            thenReturn(customerList);

        List<CustomerDTO> actualCustomerDTOs = customerService.getAll();

        Mockito.verify(customerRepository, Mockito.times(1)).
            findAll(Sort.by(Sort.Direction.ASC, "id"));
        Assertions.assertEquals(new CustomerDTO(1, "Customer 1"), actualCustomerDTOs.get(0));
        Assertions.assertEquals(new CustomerDTO(2, "Customer 2"), actualCustomerDTOs.get(1));
    }

    @Test
    void ServiceException_ifEntityDoesNotExist() {
        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> customerService.get(1000));
        Assertions.assertEquals("Could not find a customer, id 1000", throwable.getMessage());
    }

    @Test
    void updateName() {
        Customer customer = new Customer(1, "Customer 1");

        Mockito.when(customerRepository.findById(1)).thenReturn(Optional.of(customer));

        customerService.updateName(1, "Customer 2");
        Mockito.verify(customerRepository, Mockito.times(1)).findById(1);
        Mockito.verify(customerRepository, Mockito.times(1)).save(
            new Customer(1, "Customer 2"));
    }

    @Test
    void updateName_doesNothingIfNameIsTheSame() {
        Customer customer = new Customer(1, "Customer 1");

        Mockito.when(customerRepository.findById(1)).thenReturn(Optional.of(customer));

        customerService.updateName(1, "Customer 1");
        Mockito.verify(customerRepository, Mockito.times(1)).findById(1);
        Mockito.verify(customerRepository, Mockito.never()).save(Mockito.any(Customer.class));
    }

    @Test
    void updateName_throwsCorrectExceptionIfNameIsUsed() {
        String newName = "Customer 2";
        Mockito.when(customerRepository.findById(1)).thenReturn(Optional.of(new Customer(1, "Customer 1")));
        Mockito.when(customerRepository.findByName(newName)).thenReturn(new Customer(2, "Customer 2"));

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> customerService.
            updateName(1, newName));
        Assertions.assertEquals("Customer with name " + newName + " already exists",
            throwable.getMessage());
    }

    @Test
    void delete() {
        int id = 1;
        Customer customer = new Customer(id, "Customer 1");
        Mockito.when(customerRepository.findById(id)).thenReturn(Optional.of(customer));
        Mockito.when(purchaseOrderService.countByCustomer(customer)).thenReturn(0L);

        customerService.delete(id);
        Mockito.verify(customerRepository, Mockito.times(1)).deleteById(id);
    }

    @Test
    void deleteThrowsException_ifCustomerNotFound() {
        int id = 1;
        Mockito.when(customerRepository.findById(id)).thenReturn(Optional.empty());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> customerService.delete(id));
        Assertions.assertEquals("Could not find a customer to delete, id = " + id, throwable.getMessage());
    }

    @Test
    void deleteThrowsException_ifThereArePurchaseOrdersWithLinksToCustomer() {
        int id = 1;
        Customer customer = new Customer(id, "Customer 1");
        Mockito.when(customerRepository.findById(id)).thenReturn(Optional.of(customer));
        Mockito.when(purchaseOrderService.countByCustomer(customer)).thenReturn(1L);

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> customerService.delete(id));
        Assertions.assertEquals("Can not delete the customer - you have to delete purchase orders for it first, customer id = " + id, throwable.getMessage());
    }
}