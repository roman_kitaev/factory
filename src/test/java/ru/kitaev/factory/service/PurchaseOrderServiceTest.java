package ru.kitaev.factory.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import ru.kitaev.factory.domain.Customer;
import ru.kitaev.factory.domain.PurchaseOrder;
import ru.kitaev.factory.dto.CustomerDTO;
import ru.kitaev.factory.dto.PurchaseOrderDTO;
import ru.kitaev.factory.repository.PurchaseOrderRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class PurchaseOrderServiceTest {
    private PurchaseOrderService purchaseOrderService;
    @Mock
    private PurchaseOrderRepository purchaseOrderRepository;
    @Mock
    private DetailService detailService;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        purchaseOrderService = new PurchaseOrderService(purchaseOrderRepository, detailService, modelMapper);
    }

    @Test
    void save() {
        Customer customer = new Customer(1, "Customer 1");
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1", customerDTO);

        Mockito.when(modelMapper.map(customerDTO, Customer.class)).thenReturn(customer);
        Mockito.when(modelMapper.map(purchaseOrderDTO, PurchaseOrder.class)).thenReturn(purchaseOrder);

        purchaseOrderService.save(purchaseOrderDTO);
        Mockito.verify(purchaseOrderRepository, Mockito.times(1)).save(purchaseOrder);
    }

    @Test
    void save_throwsCorrectExceptionIfNumberIsUsed() {
        Customer customer = new Customer(1, "Customer 1");
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1", customerDTO);
        Mockito.when(modelMapper.map(customerDTO, Customer.class)).thenReturn(customer);
        Mockito.when(purchaseOrderRepository.findByNumberAndCustomer(purchaseOrderDTO.getNumber(), customer)).
            thenReturn(new PurchaseOrder(2, "Order 1", customer));

        Throwable throwable = Assertions.assertThrows(ServiceException.class,
            () -> purchaseOrderService.save(purchaseOrderDTO));
        Assertions.assertEquals("Purchase order with number " + purchaseOrderDTO.getNumber() +
                " and customer " + purchaseOrderDTO.getCustomer().getName() + " already exists",
            throwable.getMessage());
    }

    @Test
    void get() {
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", new Customer(1, "Customer 1"));
        Mockito.when(purchaseOrderRepository.findWithDetailsById(1)).thenReturn(purchaseOrder);

        purchaseOrderService.get(1);

        Mockito.verify(purchaseOrderRepository, Mockito.times(1)).findWithDetailsById(1);
        Mockito.verify(modelMapper, Mockito.times(1)).map(purchaseOrder, PurchaseOrderDTO.class);
    }

    @Test
    void getAll() {
        Customer customer = new Customer(1, "Customer 1");
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        List<PurchaseOrder> purchaseOrderList = Arrays.asList(
            new PurchaseOrder(1, "PO1", customer),
            new PurchaseOrder(2, "PO2", customer));
        Mockito.when(purchaseOrderRepository.findAll()).thenReturn(purchaseOrderList);

        List<PurchaseOrderDTO> actualPurchaseOrderDTOs = purchaseOrderService.getAll();

        Mockito.verify(purchaseOrderRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(new PurchaseOrderDTO(1, "PO1", customerDTO),
            actualPurchaseOrderDTOs.get(0));
        Assertions.assertEquals(new PurchaseOrderDTO(2, "PO2", customerDTO),
            actualPurchaseOrderDTOs.get(1));
    }

    @Test
    void ServiceException_ifEntityDoesNotExist() {
        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> purchaseOrderService.get(1000));
        Assertions.assertEquals("Could not find a purchase order, id 1000", throwable.getMessage());
    }

    @Test
    void update() {
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", new Customer(1, "Customer 1"));
        Mockito.when(purchaseOrderRepository.findById(1)).thenReturn(Optional.of(purchaseOrder));

        Customer newCustomer = new Customer(2, "Customer 2");
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");

        PurchaseOrder purchaseOrderToUpdate =
            new PurchaseOrder(1, "Order 2", newCustomer);
        PurchaseOrderDTO purchaseOrderDTOToUpdate =
            new PurchaseOrderDTO(1, "Order 2", newCustomerDTO);

        Mockito.when(modelMapper.map(newCustomerDTO, Customer.class)).thenReturn(newCustomer);
        Mockito.when(modelMapper.map(purchaseOrderDTOToUpdate, PurchaseOrder.class)).thenReturn(purchaseOrderToUpdate);

        purchaseOrderService.update(purchaseOrderDTOToUpdate);

        //entities are compared only by id
        Mockito.verify(purchaseOrderRepository, Mockito.times(1)).save(purchaseOrderToUpdate);
    }

    @Test
    void update_doesNothingIfNumberAndCustomerAreTheSame() {
        Customer customer = new Customer(1, "Customer 1");
        CustomerDTO customerDTO = new CustomerDTO(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1", customerDTO);

        Mockito.when(purchaseOrderRepository.findById(1)).thenReturn(Optional.of(purchaseOrder));
        Mockito.when(modelMapper.map(customerDTO, Customer.class)).thenReturn(customer);

        purchaseOrderService.update(purchaseOrderDTO);

        Mockito.verify(purchaseOrderRepository, Mockito.never()).save(Mockito.any(PurchaseOrder.class));
    }

    @Test
    void update_throwsCorrectExceptionIfPairOfNumberAndCustomerIsUsed() {
        Customer newCustomer = new Customer(2, "Customer 2");
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");
        PurchaseOrderDTO purchaseOrderDTOToUpdate = new PurchaseOrderDTO(1, "Order 2", newCustomerDTO);
        Mockito.when(purchaseOrderRepository.findById(1)).thenReturn(Optional.of(new PurchaseOrder()));
        Mockito.when(modelMapper.map(newCustomerDTO, Customer.class)).thenReturn(newCustomer);
        Mockito.when(purchaseOrderRepository.findByNumberAndCustomer("Order 2", newCustomer)).
            thenReturn(new PurchaseOrder());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> purchaseOrderService.
            update(purchaseOrderDTOToUpdate));
        Assertions.assertEquals("Purchase order with number " + "Order 2" +
            " and customer " + "Customer 2" + " already exists", throwable.getMessage());
    }

    @Test
    void delete() {
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        Mockito.when(purchaseOrderRepository.findById(1)).thenReturn(Optional.of(purchaseOrder));
        Mockito.when(detailService.countByPurchaseOrder(purchaseOrder)).thenReturn(0L);

        purchaseOrderService.delete(1);
        Mockito.verify(purchaseOrderRepository, Mockito.times(1)).deleteById(1);
    }

    @Test
    void deleteThrowsException_ifPurchaseOrderNotFound() {
        int id = 1;
        Mockito.when(purchaseOrderRepository.findById(id)).thenReturn(Optional.empty());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> purchaseOrderService.delete(id));
        Assertions.assertEquals("Could not find a purchase order to delete, id = " + id, throwable.getMessage());
    }

    @Test
    void deleteThrowsException_ifThereAreDetailsWithLinksToPurchaseOrder() {
        int id = 1;
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(id, "Order 1", customer);
        Mockito.when(purchaseOrderRepository.findById(id)).thenReturn(Optional.of(purchaseOrder));
        Mockito.when(detailService.countByPurchaseOrder(purchaseOrder)).thenReturn(1L);

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> purchaseOrderService.delete(id));
        Assertions.assertEquals("Can not delete the purchase order - " +
            "you have to delete details for it first, purchase order id = " + id, throwable.getMessage());
    }
}