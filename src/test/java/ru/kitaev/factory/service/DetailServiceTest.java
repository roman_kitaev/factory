package ru.kitaev.factory.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import ru.kitaev.factory.domain.*;
import ru.kitaev.factory.dto.*;
import ru.kitaev.factory.repository.DetailRepository;
import ru.kitaev.factory.repository.StateRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class DetailServiceTest {
    private DetailService detailService;
    @Mock
    private DetailRepository detailRepository;
    @Mock
    private StateRepository stateRepository;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        detailService = new DetailService(detailRepository, stateRepository, modelMapper);
    }

    @Test
    void save() {
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        Drawing drawing = new Drawing(1, "Drawing 1");
        State state = new State(1, "STATE 1");
        Detail detail = new Detail(1, "Det 1", drawing, purchaseOrder, state);
        DetailDTO detailDTO = new DetailDTO(1,
            detail.getNumber(),
            new DrawingDTO(1, drawing.getNumber()),
            new PurchaseOrderDTO(1, purchaseOrder.getNumber(),
                new CustomerDTO(1, customer.getName())),
            new StateDTO(1, state.getName()));

        Mockito.when(modelMapper.map(detailDTO, Detail.class)).thenReturn(detail);

        detailService.save(detailDTO);
        Mockito.verify(detailRepository, Mockito.times(1)).save(detail);
    }

    @Test
    void save_throwsCorrectExceptionIfNameIsUsed() {
        Detail detail = new Detail(2, "Detail 1",
            new Drawing(1, "Drawing 1"),
            new PurchaseOrder(1, "Order 1",
                new Customer(1, "Customer 1")),
            new State(1, "STATE 1"));

        DetailDTO detailDTO = new DetailDTO(1, "Detail 1",
            new DrawingDTO(1, "Drawing 1"),
            new PurchaseOrderDTO(1, "Order 1",
                new CustomerDTO(1, "Customer 1")),
            new StateDTO(1, "STATE 1"));
        Mockito.when(detailRepository.findByNumber(detailDTO.getNumber())).
            thenReturn(detail);

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> detailService.save(detailDTO));
        Assertions.assertEquals("Detail with number " + detail.getNumber() + " already exists",
            throwable.getMessage());
    }

    @Test
    void get() {
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        Drawing drawing = new Drawing(1, "Drawing 1");
        State state = new State(1, "STATE 1");
        Detail detail = new Detail(1, "Det 1", drawing, purchaseOrder, state);
        Mockito.when(detailRepository.findById(1)).thenReturn(Optional.of(detail));

        detailService.get(1);

        Mockito.verify(detailRepository, Mockito.times(1)).findById(1);
        Mockito.verify(modelMapper, Mockito.times(1)).map(detail, DetailDTO.class);
    }

    @Test
    void setState() {
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        Drawing drawing = new Drawing(1, "Drawing 1");
        State state = new State(1, "STATE 1");
        Detail detail = new Detail(1, "Det 1", drawing, purchaseOrder, state);
        Mockito.when(detailRepository.findById(1)).thenReturn(Optional.of(detail));
        State newState = new State(2, "State 2");
        Mockito.when(stateRepository.findByName("State 2")).thenReturn(newState);

        detailService.setState(1, "State 2");

        Mockito.verify(detailRepository, Mockito.times(1)).updateState(1, newState);
    }

    @Test
    void getAll() {
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder1 = new PurchaseOrder(1, "Order 1", customer);
        PurchaseOrder purchaseOrder2 = new PurchaseOrder(2, "Order 2", customer);
        Drawing drawing1 = new Drawing(1, "Drawing 1");
        Drawing drawing2 = new Drawing(2, "Drawing 2");
        State state1 = new State(1, "STATE 1");
        State state2 = new State(2, "STATE 2");
        Detail detail1 = new Detail(1, "Det 1", drawing1, purchaseOrder1, state1);
        Detail detail2 = new Detail(2, "Det 2", drawing2, purchaseOrder2, state2);
        List<Detail> detailList = Arrays.asList(detail1, detail2);

        Mockito.when(detailRepository.findAll()).thenReturn(detailList);

        List<DetailDTO> actualDetailDTOs = detailService.getAll();

        Mockito.verify(detailRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(new DetailDTO(1, "Det 1",
            new DrawingDTO(1, "Drawing 1"),
            new PurchaseOrderDTO(1, "Order 1",
                new CustomerDTO(1, "Customer 1")),
            new StateDTO(1, "STATE 1")), actualDetailDTOs.get(0));
        Assertions.assertEquals(new DetailDTO(2, "Det 2",
            new DrawingDTO(2, "Drawing 2"),
            new PurchaseOrderDTO(2, "Order 2",
                new CustomerDTO(1, "Customer 1")),
            new StateDTO(2, "STATE 2")), actualDetailDTOs.get(1));
    }

    @Test
    void ServiceException_ifEntityDoesNotExist() {
        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> detailService.get(1000));
        Assertions.assertEquals("Could not find a detail, id 1000", throwable.getMessage());
    }

    @Test
    void update() {
        int id = 1;
        Drawing oldDrawing = new Drawing(1, "Drawing 1");
        Customer oldCustomer = new Customer(1, "Customer 1");
        PurchaseOrder oldPurchaseOrder = new PurchaseOrder(1, "Order 1", oldCustomer);
        State state = new State(1, "initial");
        Detail oldDetail = new Detail(id, "Detail 1", oldDrawing, oldPurchaseOrder, state);

        Drawing newDrawing = new Drawing(2, "Drawing 2");
        Customer newCustomer = new Customer(2, "Customer 2");
        PurchaseOrder newPurchaseOrder = new PurchaseOrder(2, "Order 2", newCustomer);
        State newState = new State(2, "prepare");
        Detail detailToUpdate = new Detail(id, "Detail 2", newDrawing, newPurchaseOrder, newState);

        DrawingDTO newDrawingDTO = new DrawingDTO(2, "Drawing 2");
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");
        PurchaseOrderDTO newPurchaseOrderDTO = new PurchaseOrderDTO(2, "Order 2", newCustomerDTO);
        StateDTO newStateDTO = new StateDTO(2, "prepare");
        DetailDTO detailDTOToUpdate = new DetailDTO(id, "Detail 2", newDrawingDTO,
            newPurchaseOrderDTO, newStateDTO);

        Mockito.when(detailRepository.findById(id)).thenReturn(Optional.of(oldDetail));
        Mockito.when(detailRepository.findByNumber("Detail 2")).thenReturn(null);
        Mockito.when(modelMapper.map(detailDTOToUpdate, Detail.class)).thenReturn(detailToUpdate);

        detailService.update(detailDTOToUpdate);

        //entities are compared only by id
        Mockito.verify(detailRepository, Mockito.times(1)).save(detailToUpdate);
    }

    @Test
    void update_throwsCorrectExceptionIfNumberIsUsed() {
        int id = 1;
        Drawing drawing = new Drawing(1, "Drawing 1");
        Customer customer = new Customer(1, "Customer 1");
        PurchaseOrder purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        State state = new State(1, "initial");
        Detail detail = new Detail(id, "Detail 1", drawing, purchaseOrder, state);
        Mockito.when(detailRepository.findById(id)).thenReturn(Optional.of(detail));

        String newNumber = "Detail 2";
        DrawingDTO newDrawingDTO = new DrawingDTO(2, "Drawing 2");
        CustomerDTO newCustomerDTO = new CustomerDTO(2, "Customer 2");
        PurchaseOrderDTO newPurchaseOrderDTO = new PurchaseOrderDTO(2, "Order 2", newCustomerDTO);
        StateDTO newStateDTO = new StateDTO(2, "prepare");
        DetailDTO detailDTOToUpdate = new DetailDTO(id, newNumber, newDrawingDTO,
            newPurchaseOrderDTO, newStateDTO);
        Mockito.when(detailRepository.findByNumber(newNumber)).thenReturn(new Detail());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> detailService.
            update(detailDTOToUpdate));
        Assertions.assertEquals("Detail with number " + newNumber + " already exists", throwable.getMessage());
    }

    @Test
    void delete() {
        Drawing drawing = new Drawing(2, "Drawing 2");
        Customer customer = new Customer(2, "Customer 2");
        PurchaseOrder purchaseOrder = new PurchaseOrder(2, "Order 2", customer);
        State state = new State(2, "prepare");

        Detail detail = new Detail(1, "Detail 2", drawing, purchaseOrder, state);
        Mockito.when(detailRepository.findById(1)).thenReturn(Optional.of(detail));

        detailService.delete(1);
        Mockito.verify(detailRepository, Mockito.times(1)).deleteById(1);
    }

    @Test
    void deleteThrowsException_ifCustomerNotFound() {
        Mockito.when(detailRepository.findById(1)).thenReturn(Optional.empty());

        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> detailService.delete(1));
        Assertions.assertEquals("Could not find a detail to delete, id = " + 1, throwable.getMessage());
    }
}
