package ru.kitaev.factory.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import ru.kitaev.factory.domain.State;
import ru.kitaev.factory.dto.StateDTO;
import ru.kitaev.factory.repository.StateRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class StateServiceTest {
    private StateService stateService;
    @Mock
    private StateRepository stateRepository;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        stateService = new StateService(stateRepository, modelMapper);
    }

    @Test
    void save() {
        State state = new State(1, "STATE 1");
        StateDTO stateDTO = new StateDTO(1, state.getName());

        Mockito.when(modelMapper.map(stateDTO, State.class)).thenReturn(state);

        stateService.save(stateDTO);
        Mockito.verify(stateRepository, Mockito.times(1)).save(state);
    }

    @Test
    void getByName() {
        State state = new State(1, "STATE 1");
        Mockito.when(stateRepository.findByName("STATE 1")).thenReturn(state);

        stateService.getByName("STATE 1");

        Mockito.verify(stateRepository, Mockito.times(1)).findByName("STATE 1");
        Mockito.verify(modelMapper, Mockito.times(1)).map(state, StateDTO.class);
    }

    @Test
    void getById() {
        State state = new State(1, "STATE 1");
        Mockito.when(stateRepository.findById(1)).thenReturn(Optional.of(state));

        stateService.getById(1);

        Mockito.verify(stateRepository, Mockito.times(1)).findById(1);
        Mockito.verify(modelMapper, Mockito.times(1)).map(state, StateDTO.class);
    }

    @Test
    void ServiceException_ifEntityDoesNotExist() {
        Throwable throwable = Assertions.assertThrows(ServiceException.class, () -> stateService.getByName("123"));
        Assertions.assertEquals("Could not find a state, name 123", throwable.getMessage());
    }

    @Test
    void getAll() {
        List<State> stateList = Arrays.asList(
            new State(1, "state 1"), new State(2, "state 2"));
        Mockito.when(stateRepository.findAll()).thenReturn(stateList);
        Mockito.when(modelMapper.map(stateList.get(0), StateDTO.class)).thenReturn(new StateDTO(1, "state 1"));
        Mockito.when(modelMapper.map(stateList.get(1), StateDTO.class)).thenReturn(new StateDTO(2, "state 2"));

        List<StateDTO> actualStateDTOs = stateService.getAll();

        Mockito.verify(stateRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(new StateDTO(1, "state 1"), actualStateDTOs.get(0));
        Assertions.assertEquals(new StateDTO(2, "state 2"), actualStateDTOs.get(1));
    }
}