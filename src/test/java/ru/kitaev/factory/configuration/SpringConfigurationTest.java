package ru.kitaev.factory.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.kitaev.factory.domain.*;
import ru.kitaev.factory.dto.*;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(classes = SpringConfiguration.class)
class SpringConfigurationTest {
    @Autowired
    private ModelMapper modelMapper;

    private Customer customer;
    private CustomerDTO customerDTO;

    private Drawing drawing;
    private DrawingDTO drawingDTO;

    private State state;
    private StateDTO stateDTO;

    private PurchaseOrder purchaseOrder;
    private PurchaseOrderDTO purchaseOrderDTO;

    private Detail detail;
    private DetailDTO detailDTO;

    @BeforeEach
    void setUp() {
        customer = new Customer(1, "Customer 1");
        customerDTO = new CustomerDTO(1, "Customer 1");

        drawing = new Drawing(1, "Drawing 1");
        drawingDTO = new DrawingDTO(1, "Drawing 1");

        state = new State(1, "State 1");
        stateDTO = new StateDTO(1, "State 1");

        purchaseOrder = new PurchaseOrder(1, "Order 1", customer);
        purchaseOrderDTO = new PurchaseOrderDTO(1, "Order 1", customerDTO);

        detail = new Detail(1, "Detail 1", drawing, purchaseOrder, state);
        detailDTO = new DetailDTO(1, "Detail 1", drawingDTO, purchaseOrderDTO, stateDTO);
    }

    @Test
    void modelMapper_mapsCustomerWithPurchaseOrdersAndWithoutDetails() {
        CustomerDTO expectedCustomerDTO = customerDTO;

        List<PurchaseOrder> purchaseOrderList = Arrays.asList(
            new PurchaseOrder(1, "Order 1", customer),
            new PurchaseOrder(2, "Order 2", customer),
            new PurchaseOrder(3, "Order 3", customer));
        List<PurchaseOrderDTO> purchaseOrderDTOList = Arrays.asList(
            new PurchaseOrderDTO(1, "Order 1", expectedCustomerDTO),
            new PurchaseOrderDTO(2, "Order 2", expectedCustomerDTO),
            new PurchaseOrderDTO(3, "Order 3", expectedCustomerDTO));
        customer.getPurchaseOrderList().addAll(purchaseOrderList);
        expectedCustomerDTO.getPurchaseOrderList().addAll(purchaseOrderDTOList);

        List<Detail> detailList = Arrays.asList(
            new Detail(1, "Detail 1", drawing, purchaseOrderList.get(0), state),
            new Detail(2, "Detail 2", drawing, purchaseOrderList.get(0), state));
        List<DetailDTO> detailDTOList = Arrays.asList(
            new DetailDTO(1, "Detail 1", drawingDTO, purchaseOrderDTOList.get(0), stateDTO),
            new DetailDTO(2, "Detail 2", drawingDTO, purchaseOrderDTOList.get(0), stateDTO));
        customer.getPurchaseOrderList().get(0).getDetailList().addAll(detailList);
        expectedCustomerDTO.getPurchaseOrderList().get(0).getDetailList().addAll(detailDTOList);

        CustomerDTO actualCustomerDTO = modelMapper.map(customer, CustomerDTO.class);

        Assertions.assertEquals(expectedCustomerDTO.getId(), actualCustomerDTO.getId());
        Assertions.assertEquals(expectedCustomerDTO.getName(), actualCustomerDTO.getName());
        for (int i = 0; i < 3; i++) {
            Assertions.assertEquals(
                expectedCustomerDTO.getPurchaseOrderList().get(i).getId(),
                actualCustomerDTO.getPurchaseOrderList().get(i).getId());
            Assertions.assertEquals(
                expectedCustomerDTO.getPurchaseOrderList().get(i).getNumber(),
                actualCustomerDTO.getPurchaseOrderList().get(i).getNumber());
        }

        Assertions.assertEquals(0,
            actualCustomerDTO.getPurchaseOrderList().get(0).getDetailList().size());
    }

    @Test
    void modelMapper_mapsPurchaseOrdersWithDetailsAndCustomerWithoutPurchaseOrders() {
        PurchaseOrderDTO expectedPurchaseOrderDTO = purchaseOrderDTO;

        customer.getPurchaseOrderList().add(purchaseOrder);
        customerDTO.getPurchaseOrderList().add(expectedPurchaseOrderDTO);

        List<Detail> detailList = Arrays.asList(
            new Detail(1, "Detail 1", drawing, purchaseOrder, state),
            new Detail(2, "Detail 2", drawing, purchaseOrder, state));
        List<DetailDTO> detailDTOList = Arrays.asList(
            new DetailDTO(1, "Detail 1", drawingDTO, expectedPurchaseOrderDTO, stateDTO),
            new DetailDTO(2, "Detail 2", drawingDTO, expectedPurchaseOrderDTO, stateDTO));
        purchaseOrder.getDetailList().addAll(detailList);
        expectedPurchaseOrderDTO.getDetailList().addAll(detailDTOList);

        PurchaseOrderDTO actualPurchaseOrderDTO = modelMapper.map(purchaseOrder, PurchaseOrderDTO.class);

        Assertions.assertEquals(expectedPurchaseOrderDTO.getId(), actualPurchaseOrderDTO.getId());
        Assertions.assertEquals(expectedPurchaseOrderDTO.getNumber(), actualPurchaseOrderDTO.getNumber());
        Assertions.assertEquals(
            expectedPurchaseOrderDTO.getCustomer().getId(),
            actualPurchaseOrderDTO.getCustomer().getId());
        Assertions.assertEquals(
            expectedPurchaseOrderDTO.getCustomer().getName(),
            actualPurchaseOrderDTO.getCustomer().getName());
        for (int i = 0; i < 2; i++) {
            Assertions.assertEquals(
                expectedPurchaseOrderDTO.getDetailList().get(i).getId(),
                actualPurchaseOrderDTO.getDetailList().get(i).getId());
            Assertions.assertEquals(
                expectedPurchaseOrderDTO.getDetailList().get(i).getNumber(),
                actualPurchaseOrderDTO.getDetailList().get(i).getNumber());
            Assertions.assertEquals(
                expectedPurchaseOrderDTO.getDetailList().get(i).getDrawing(),
                actualPurchaseOrderDTO.getDetailList().get(i).getDrawing());
        }

        Assertions.assertEquals(0,
            actualPurchaseOrderDTO.getCustomer().getPurchaseOrderList().size());
    }

    @Test
    void modelMapper_mapsDetailAndPurchaseOrderWithoutDetailAndCustomerWithoutPurchaseOrders() {
        DetailDTO expectedDetailDTO = detailDTO;

        DetailDTO actualDetailDTO = modelMapper.map(detail, DetailDTO.class);

        Assertions.assertEquals(expectedDetailDTO.getId(), actualDetailDTO.getId());
        Assertions.assertEquals(expectedDetailDTO.getNumber(), actualDetailDTO.getNumber());
        Assertions.assertEquals(expectedDetailDTO.getDrawing(), actualDetailDTO.getDrawing());
        Assertions.assertEquals(expectedDetailDTO.getState(), actualDetailDTO.getState());

        Assertions.assertEquals(0,
            actualDetailDTO.getPurchaseOrder().getDetailList().size());
        Assertions.assertEquals(0,
            actualDetailDTO.getPurchaseOrder().getCustomer().getPurchaseOrderList().size());
    }

    @Test
    void modelMapper_mapsDrawings() {
        Drawing drawing = new Drawing(1, "Drawing 1");
        Assertions.assertEquals(new DrawingDTO(1, "Drawing 1"), modelMapper.map(drawing, DrawingDTO.class));
    }

    @Test
    void modelMapper_mapsStates() {
        State state = new State(1, "State 1");
        Assertions.assertEquals(new StateDTO(1, "State 1"), modelMapper.map(state, StateDTO.class));
    }
}